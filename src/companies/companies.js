import React, {useState, useRef, useEffect} from "react";
import { Link, Redirect } from "react-router-dom";
import { useGlobal} from 'reactn';

import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


import Modal from '@material-ui/core/Modal';


//import TextField from '@material-ui/core/TextField';
//import InputAdornment from '@material-ui/core/InputAdornment';

import useDataApi from "../use_data_api/usedataapi"
import axios from 'axios';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  card: {
    margin: theme.spacing(1),
  },
}));


function Companies() {

    const classes = useStyles();

    const [global, setGlobal] = useGlobal();
    const [modalOpen, setModalOpen] = useState(false);
    const [modalCompany, setModalcompany] = useState({contact:{phone:"",name:"",email:""},details:{company:{name:"", incorporation_locality:"", trading_name:"", registration_number:"", date_onboarded:"",registration_address:{address_iso_country:"",address_postal_code:"",address_region:"",address_street:"",address_refinement:"",address_number:"",address_city:""},trading_addresses:[{address_iso_country:"",address_postal_code:"",address_region:"",address_street:"",address_refinement:"",address_number:"",address_city:""}],directors:[{company:{name:""}},{person:{name:"",date_of_birth:"",pep:false,pep_type:"",pep_notes:""}}],ultimate_beneficial_owners:[{person:{pep_notes:"",address:{address_iso_country:"",address_postal_code: "",address_region: "",address_street: "",address_refinement: "",address_number: "",address_city: ""},nationality: [""],date_of_birth: "",pep_type: "",name: "",pep: false}}, {company:{name: "",registration_address:{address_iso_country: "",address_postal_code: "",address_region: "",address_street: "",address_refinement: "",address_number: "",address_city: ""}}}],stock_exchanges:[{stock_exchange_name: "",stock_exchange_ticker: ""}]}}});
    const [kycDialogOpen, setKYCdialogOpen] = useState(false);
    const [companyId, setCompanyid] = useState(0);
    const [modalOpen1, setModalOpen1] = useState(false);
    const [modalURL, setModalURL] = useState("");


    const valid_until = useRef(null);
    const company_id = useRef(null);

    const [{ data, isLoading, isError }, doFetch] = useDataApi(
      'http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler.jsp?callType=get_companies',
      [{details:{company:{name:"", incorporation_locality:""}},local_details:{mykycbank:"",approved:0, kyc_doc:"",valid_until:""}}]
    );  

    const handleModalOpen = (i) => {
      setModalcompany(data[i])
      setModalOpen(true);
    };
  
    const handleModalClose = () => {
      setModalOpen(false);
    };

    function approve(company_id){

      doFetch(
        encodeURI('http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler.jsp?callType=approve_company&company_id=' + company_id),
      );

    }

    const updateValiduntil = e => {
      valid_until.current.value = e.target.value
      //console.log(valid_until.current.value)
    };


    function openKYCdialog(company_id) {
      setCompanyid(company_id);
      setKYCdialogOpen(true);
    }
  
    function closeKYCdialog() {
      setKYCdialogOpen(false);
    }

    function handleFileInput(e)
    {

      let fileName = e.target.files[0].name

      let fd = new FormData();

      fd.append("file",e.target.files[0]);

      axios.post("http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/upload.jsp", fd, { // receive two parameter endpoint url ,form data 
      })
      .then(res => { // then print response status
        //console.log(res)
        doFetch(
          encodeURI('http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler.jsp?callType=update_kyc&company_id=' + companyId + '&file_name=' + fileName + '&valid_until=' + valid_until.current.value),
        );
        closeKYCdialog()  
      })

    }

    const handleModalOpen1 = (url) => {
      setModalURL(url)
      setModalOpen1(true);
    };
  
    const handleModalClose1 = () => {
      setModalOpen1(false);
    };

    if (global.user_id == 1 || true){

      return  <Box>
          <Modal open={modalOpen1} onClose={handleModalClose1}>
            <div style={{width:"70%",height:"80%",left:"15%",top:"10%",position:"absolute"}}>
              <iframe src={modalURL} width="100%" height="100%"></iframe>
            </div>
          </Modal>
          <br/><br/><span className="pagetitle">Companies</span>
          <Dialog open={kycDialogOpen} onClose={closeKYCdialog} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">KYC Document</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Please set "Valid until" date and then upload KYC document 
              </DialogContentText>
              <TextField
                  id="valid_until"
                  inputRef={valid_until}
                  label="Valid until"
                  onChange={updateValiduntil}
                  type="date"
                  InputProps={{
                  startAdornment: (
                      <InputAdornment position="start">
                          <Icon>event</Icon>
                      </InputAdornment>
                  ),
                  }}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={closeKYCdialog} color="primary">
                Cancel
              </Button>
              <span>
                <input
                className={classes.input}
                style={{ display: 'none' }}
                id="raised-button-file"
                multiple
                type="file"
                onChange={ (e) => handleFileInput(e) }
                />
                <label htmlFor="raised-button-file">
                  <Button variant="outlined" color="primary" component="span" className={classes.button}>
                    Upload KYC Doc
                  </Button>
                </label> 
              </span>
            </DialogActions>
          </Dialog>
          <Modal open={modalOpen} onClose={handleModalClose}>
            <div style={{width:"50%",height:"80%",left:"25%",top:"10%",position:"absolute",backgroundColor:"#ffffff",padding:"10px",overflow:"scroll"}}>
              <span className="pagetitle">
                {modalCompany.details.company.name}, {modalCompany.details.company.incorporation_locality}
              </span>
              <br/><br/>
              <span>
              <span className="subtitle">Contact:</span>
                <br/>
                <span className="grey_it">Name:</span> {modalCompany.contact.name}
                <br/>
                <span className="grey_it">Phone:</span> {modalCompany.contact.phone}
                <br/>
                <span className="grey_it">Email:</span> {modalCompany.contact.email}
              </span>
              <br/><br/>
              <span>
              <span className="subtitle">Company details:</span>
                <br/>
                <span className="grey_it">Trading Name:</span> {modalCompany.details.company.trading_name}
                <br/>
                <span className="grey_it">Registration #:</span> {modalCompany.details.company.registration_number}
                <br/>
                <span className="grey_it">Date Onboarded:</span> {modalCompany.details.company.date_onboarded}
              </span>
              <br/><br/>
              <span>
              <span className="subtitle">Registration Address:</span>
                <br/>
                <span className="grey_it">Street:</span> {modalCompany.details.company.registration_address.address_street}
                <br/>
                <span className="grey_it">#:</span> {modalCompany.details.company.registration_address.address_number}
                <br/>
                <span className="grey_it">Postal code:</span> {modalCompany.details.company.registration_address.address_postal_code}
                <br/>
                <span className="grey_it">City:</span> {modalCompany.details.company.registration_address.address_city}
                <br/>
                <span className="grey_it">Region:</span> {modalCompany.details.company.registration_address.address_region}
                <br/>
                <span className="grey_it">Country ISO:</span> {modalCompany.details.company.registration_address.address_iso_country}
                <br/>
                <span className="grey_it">Refinement:</span> {modalCompany.details.company.registration_address.address_refinement}
              </span>
              {modalCompany.details.company.trading_addresses.map((address,index)=>{
                return <span key={index}>
                    <br/><br/>
                    <span className="subtitle">Trading Address:</span>
                    <br/>
                    <span className="grey_it">Street:</span> {address.address_street}
                    <br/>
                    <span className="grey_it">#:</span> {address.address_number}
                    <br/>
                    <span className="grey_it">Postal code:</span> {address.address_postal_code}
                    <br/>
                    <span className="grey_it">City:</span> {address.address_city}
                    <br/>
                    <span className="grey_it">Region:</span> {address.address_region}
                    <br/>
                    <span className="grey_it">Country ISO:</span> {address.address_iso_country}
                    <br/>
                    <span className="grey_it">Refinement:</span> {address.address_refinement}
                  </span>
              })}
              {modalCompany.details.company.directors.map((director,index)=>{
                return <span key={index}>
                    <br/><br/>
                    <span className="subtitle">Director:</span>
                    {typeof director.company !== 'undefined' ? (
                      <span>
                        <br/>
                        <span className="grey_it">Company Name:</span> {director.company.name}
                      </span>
                    ) : (
                      <span>
                        <br/>
                        <span className="grey_it">Person Name:</span> {director.person.name}
                        <br/>
                        <span className="grey_it">Date of Birth:</span> {director.person.date_of_birth}
                        <br/>
                        <span className="grey_it">PEP:</span> {director.person.pep.toString()}
                        <br/>
                        <span className="grey_it">PEP Type:</span> {director.person.pep_type}
                        <br/>
                        <span className="grey_it">PEP Notes:</span> {director.person.pep_notes}
                      </span>
                    )}
                  </span>
              })}
              {modalCompany.details.company.ultimate_beneficial_owners.map((ubo,index)=>{
                return <span key={index}>
                    <br/><br/>
                    <span className="subtitle">Ultimate Beneficial Owner:</span>
                    {typeof ubo.company !== 'undefined' ? (
                      <span>
                        <br/>
                        <span className="grey_it">Company Name:</span> {ubo.company.name}
                        <br/>
                        <span>
                          Registration Address:
                          <br/>
                          <span className="grey_it">Street:</span> {ubo.company.registration_address.address_street}
                          <br/>
                          <span className="grey_it">#:</span> {ubo.company.registration_address.address_number}
                          <br/>
                          <span className="grey_it">Postal code:</span> {ubo.company.registration_address.address_postal_code}
                          <br/>
                          <span className="grey_it">City:</span> {ubo.company.registration_address.address_city}
                          <br/>
                          <span className="grey_it">Region:</span> {ubo.company.registration_address.address_region}
                          <br/>
                          <span className="grey_it">Country ISO:</span> {ubo.company.registration_address.address_iso_country}
                          <br/>
                          <span className="grey_it">Refinement:</span> {ubo.company.registration_address.address_refinement}
                        </span>
                      </span>
                    ) : (
                      <span>
                        <br/>
                        <span className="grey_it">Person Name:</span> {ubo.person.name}
                        <br/>
                        <span className="grey_it">Date of Birth:</span> {ubo.person.date_of_birth}
                        <br/>
                        <span className="grey_it">PEP:</span> {ubo.person.pep.toString()}
                        <br/>
                        <span className="grey_it">PEP Type:</span> {ubo.person.pep_type}
                        <br/>
                        <span className="grey_it">PEP Notes:</span> {ubo.person.pep_notes}
                        <br/>
                        <span className="grey_it">Nationality:</span> {ubo.person.nationality.map((nationality,index)=>{return <span key={index}>{nationality},</span>})}
                        <span>
                          Address:
                          <br/>
                          <span className="grey_it">Street:</span> {ubo.person.address.address_street}
                          <br/>
                          <span className="grey_it">#:</span> {ubo.person.address.address_number}
                          <br/>
                          <span className="grey_it">Postal code:</span> {ubo.person.address.address_postal_code}
                          <br/>
                          <span className="grey_it">City:</span> {ubo.person.address.address_city}
                          <br/>
                          <span className="grey_it">Region:</span> {ubo.person.address.address_region}
                          <br/>
                          <span className="grey_it">Country ISO:</span> {ubo.person.address.address_iso_country}
                          <br/>
                          <span className="grey_it">Refinement:</span> {ubo.person.address.address_refinement}
                        </span>
                      </span>
                    )}
                  </span>
              })}
              {modalCompany.details.company.stock_exchanges.map((exchange,index)=>{
                return <span key={index}>
                    <br/><br/>
                    <span className="subtitle">Stock Exchange:</span>
                    <br/>
                    <span className="grey_it">Name:</span> {exchange.stock_exchange_name}
                    <br/>
                    <span className="grey_it">Ticker:</span> {exchange.stock_exchange_ticker}
                  </span>
              })}
            </div>
          </Modal>

          <br/><br/>
          {isLoading ? (
            <span><CircularProgress /></span>
          ) : (
            data.map((company,index)=>{
            return <Card key={index} className={classes.card}>
                      <CardContent>
                        <span>
                          {company.details.company.name}, {company.details.company.incorporation_locality} <span className="grey_it">(MyKYCBank profile id: {company.local_details.mykycbank})</span>
                        </span>
                      </CardContent>
                      <CardActions>
                        <Button variant="outlined" color="primary" onClick={()=>handleModalOpen(index)}>Details</Button>
                        <Button variant="outlined" color="primary" onClick={()=>openKYCdialog(company.local_details.company_id)}>Upload KYC</Button>
                        {company.local_details.kyc_doc != "" ? (
                          <Button variant="outlined" color="primary" onClick={()=>handleModalOpen1("http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/uploaded/" + company.local_details.kyc_doc)}>KYC valid until: {company.local_details.valid_until.substring(0, 10)}</Button>
                        ) : (
                          ""
                        )}
                        {company.local_details.approved != 1 ? (
                          <Button variant="outlined" color="primary" onClick={()=>approve(company.local_details.company_id)}>Approve</Button>
                        ) : (
                          ""
                        )}
                      </CardActions>
                    </Card>
            })
          )}

        </Box>;

    }else{

      return <Redirect to='/' />

    }

}

  export default Companies;
  