import React from 'react';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";

import Diamond_png from './images/polished-diamond.png';
import el_logo from './images/EL_logo_email.png';
import railsbank from './images/railsbank.jpg';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';

import Login from "./login/login"
import Companies from "./companies/companies"


function App() {
  return (
    <Router>
      <div>
        <Header />
        <Container>

          <Route exact path="/" component={Login} />
          <Route path="/companies" component={Companies} />

        </Container>

        <br/><br/><br/><br/>
        <Footer />

      </div>
    </Router>
  );
}

function Header() {
  return (
      <center>
        <Box width="100%" bgcolor="#243743"><br/></Box>
        <font size="+2" color="#243743">
        Admin - Trusted Members Diamond Market
        </font> <sup className="grey_it">POC</sup>
        <img alt="Diamond" src={Diamond_png} width="100px" style={{position:'relative',left:'20px',top:'35px'}}></img>
      </center>
  );
}

function Footer(){
  return <center>
          Powered by
          <br/>
          <img alt="Everledger" src={el_logo}></img>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <img alt="Railsbank" src={railsbank} width="215px" style={{position:'relative',top:'15px'}}></img>
        </center>
}


export default App;
